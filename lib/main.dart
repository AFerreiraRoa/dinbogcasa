import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
 
import 'package:dinbog/src/pages/home.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
    .then((_) {
      runApp(new MyApp());
    });
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {  
    return MaterialApp(
      theme: ThemeData(
        textTheme: TextTheme(
          body1: TextStyle(
            fontSize: 11
          ),
        ),
        buttonTheme: ButtonThemeData(
          height: 25,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5)
          ),
        ),
        primaryIconTheme: IconThemeData(
          color: Colors.grey,
          size: 16
        ),
        appBarTheme: AppBarTheme(
          color: Colors.white,
          brightness: Brightness.dark,
          textTheme: TextTheme(
            title: TextStyle(
              color: Colors.black,
              fontSize: 17
            )
          )
        ),
        fontFamily: "Roboto"
      ),
      title: 'Dinbog',
      home: Home(),
      debugShowCheckedModeBanner: false,
    );
  }
}
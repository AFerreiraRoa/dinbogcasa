import 'package:dinbog/src/pages/tabs/casting.dart';
import 'package:dinbog/src/pages/tabs/search.dart';
import 'package:dinbog/src/pages/tabs/upBook.dart';
import 'package:flutter/material.dart';
import 'package:dinbog/src/pages/tabs/perfil.dart';

import 'package:badges/badges.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  

    @override
   void initState() {
     super.initState();
     
   }

    @override
    void dispose() {
      super.dispose();
      
    }

  int currentPage = 0;

  @override
  Widget build(BuildContext context) {

    //Items TabBar

    final _bottomNavigationBarItems = <BottomNavigationBarItem> [
      BottomNavigationBarItem(icon: Icon(Icons.home),title: SizedBox.shrink()),
      BottomNavigationBarItem(icon: Icon(Icons.search),title:  SizedBox.shrink()),
      BottomNavigationBarItem(icon: Icon(Icons.add_circle_outline),title:  SizedBox.shrink()),
      BottomNavigationBarItem(icon: Badge(
        padding: EdgeInsets.symmetric(horizontal: 5,vertical: 2),
        position: BadgePosition.topRight(right: -3,top: -5),
        shape: BadgeShape.square,
        borderRadius: 5.0,
        badgeContent: Text("4",style: TextStyle(color: Colors.white),),
        child: Icon(Icons.notifications_none),
      ),title:  SizedBox.shrink()),
      BottomNavigationBarItem(icon: Icon(Icons.movie),title:  SizedBox.shrink()),
    ];

    //Aquí comienza el armado de la vista (scaffold)
    return Scaffold(
      body: _callPage(currentPage),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 16,
        unselectedIconTheme: IconThemeData(color: Colors.grey),
        selectedIconTheme: IconThemeData(color: Colors.blue),
        items: _bottomNavigationBarItems,
        currentIndex: currentPage,
        type: BottomNavigationBarType.fixed,
        iconSize: 28,
        onTap: (int index){
          setState(() {
            currentPage = index;
          });
        },
      ),
    );
  }

  Widget _callPage( int currentPage) {

    switch (currentPage) {

      case 0: return Perfil();
      case 1: return Search();
      case 2: return UpBook();
      case 4: return Casting();

      default: return Perfil();

    }
  }

}
import 'package:flutter/material.dart';

class UpBook extends StatefulWidget {
  @override
  _UpBookState createState() => _UpBookState();
}

class _UpBookState extends State<UpBook> {

  bool _switchVal = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Center(
          child: Text("Upload book", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0)),
        ),
        leading: Icon(Icons.arrow_back, color: Colors.white, size: 30.0),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.all(15.0),
            child: Text("Next",style: TextStyle(color: Colors.white,fontSize: 18))
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              child: Image(
                image: AssetImage("assets/images/modelo_manos.jpg"),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:30.0, left: 30.0,right: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
                      text: "Title",
                      children: <TextSpan>[
                        TextSpan(
                          style: TextStyle(color: Colors.red),
                          text: "*"
                        )
                      ]
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: EdgeInsets.only(bottom: 30.0),
                    child: TextFormField(
                      decoration: InputDecoration(

                      ),
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
                      text: "Description",
                      children: <TextSpan>[
                        TextSpan(
                          style: TextStyle(color: Colors.red),
                          text: "*"
                        )
                      ]
                    ),
                  ),
                  Container(
                    height: 30,
                    margin: EdgeInsets.only(bottom: 30.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        
                      ),
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
                      text: "Category",
                      children: <TextSpan>[
                        TextSpan(
                          style: TextStyle(color: Colors.red),
                          text: "*"
                        )
                      ]
                    ),
                  ),
                  Container(
                    height: 30,
                    child: TextFormField(
                      decoration: InputDecoration(
                        
                      ),
                    ),
                  ),
                  
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: ListTile(
                title: Text("Is this book private?",style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 14.0)),
                subtitle: Text("Only visible by your friends",style: TextStyle(color: Colors.grey)),
                trailing: Switch(
                  inactiveTrackColor: Color(0xFFd4d4d4),
                  onChanged: (bool value) {
                    setState(() {
                      this._switchVal = value;
                    });
                  },
                  value: _switchVal,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30.0),
              child: Text("Upload yor photos on your book", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 14)),
            ),
            Container(
              margin: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 20.0),
              child: Table(
                children: [
                  TableRow(
                    children: [
                      addPhoto(),
                      photo(),
                      photo(),
                    ]
                  ),
                  TableRow(
                    children: [
                      photo(),
                      photo(),
                      photo(),
                    ]
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

    Widget addPhoto() {

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Color(0xFFe8e8e8)
      ),
      margin: EdgeInsets.all(10.0),
      height: 100,
      width: 100,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5.0),
        child: Icon(Icons.add,color: Color(0xFFd4d4d4),size: 50,)
      ),
    );
  }

    Widget photo() {

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
      ),
      margin: EdgeInsets.all(10),
      height: 100,
      width: 100,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5.0),
        child: Image(
          image: AssetImage("assets/images/modelo_cuadrado.jpg"),
        ),
      ),
    );
  }




}
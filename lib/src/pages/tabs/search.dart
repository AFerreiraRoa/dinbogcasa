import 'package:flutter/material.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
       actions: <Widget>[
        Container(
          margin: EdgeInsets.only(right: 15),
          child: Icon(
             Icons.menu,
             size: 30,
             color: Colors.blue,
          )
        ) 
       ],
       title: Theme(
        child: Container(
            height: 35,
            width: 300,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50)
           ),
           child: TextField(
              style: TextStyle(
                color: Color(0xFF828282),
              ), 
              maxLines: 1,
              minLines: 1,
              cursorColor: Color(0xFF828282),              
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(bottom: 5),
                suffixIcon: Icon(Icons.clear), //TODO Cambiar a iconButton
                prefixIcon: Icon(Icons.search), //TODO Cambiar a iconButton
                filled: true,
                fillColor: Color(0xFFd4d4d4),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  borderSide: BorderSide(color: Colors.white),
                )
              ),
           ),
        ),
         data: Theme.of(context).copyWith(primaryColor: Color(0xFF828282))
       ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 25.0,bottom: 10.0),
              child: Center(
                child: Text("Suggestions",style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
              ),
            ),
            _crearTabla()
          ],
        ),
      ),
    );
  }

  Widget _crearTabla() {

    return Table(

      children: [
        TableRow(
          children: [
            _suggestion(), // TODO para hacer que todos las sugerencias sean diferentes tienes que pasarle por argumento
            _suggestion(), // los valores que van a utilizar cada uno, en el curso de fernando explica como hacerlo 
            _suggestion()  // clase 142 :D y sustituir el valor estatico de abajo por lo que vas a enviar por argumento
          ]
        ),
        TableRow(
          children: [
            _suggestion(),
            _suggestion(),
            _suggestion()
          ]
        ),
        TableRow(
          children: [
            _suggestion(),
            _suggestion(),
            _suggestion()
          ]
        ),
        TableRow(
          children: [
            _suggestion(),
            _suggestion(),
            _suggestion()
          ]
        ),
        TableRow(
          children: [
            _suggestion(),
            _suggestion(),
            _suggestion()
          ]
        ),
        TableRow(
          children: [
            _suggestion(),
            _suggestion(),
            _suggestion()
          ]
        ),
      ]
    );
  }

  Widget _suggestion() {

    return Container(
      margin: EdgeInsets.all(15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5.0),
            
            child: CircleAvatar(
              
              radius: 45,
              backgroundImage: AssetImage("assets/images/modelo_perfil.jpg"),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(45), // para que la sombra no se salga del circleAvatar 
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 10.0, // has the effect of softening the shadow
                  spreadRadius: -2.0, // has the effect of extending the shadow
                  offset: Offset(
                    0.0, // horizontal, move right 10
                    5.0, // vertical, move down 10
                  ),
                )
              ]
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 1.0),
            child: Text("Karla Vemon")
          ),
          Text("Model",style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold)),
          Container(
            width: 70,
            child: RaisedButton(
              elevation: 4,
              color: Colors.blue,
              child: Text("Follow", style: TextStyle(color: Colors.white,fontSize: 11)),
              onPressed: (){},
            ),
          )
        ],
      ),
    );

  }

}
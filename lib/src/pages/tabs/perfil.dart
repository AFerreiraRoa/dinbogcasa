import 'package:flutter/material.dart';

class Perfil extends StatefulWidget {
  @override
  _PerfilState createState() => _PerfilState();
}

class _PerfilState extends State<Perfil> {

  double _fontSizeButton = 11;

  String _nombrePerfil = "Karla Venom ";
  String _commentPost = "Today was a excellent photoshoot day!!";
  String _nComment = "6";


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 6,
        leading: Icon(Icons.arrow_back, size: 30),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 60,
            ),
            Text("$_nombrePerfil ",style: TextStyle(fontWeight: FontWeight.bold),),
            Icon(Icons.check_circle, color: Colors.blue)
          ],
        ),
      ),
      body: SingleChildScrollView(
      child: DefaultTabController(
        length: 6,
        child: Column(
          children: <Widget>[
            _infoPerfil(),
            _tabBar(),
            _postPerfilListTile(),
            _postPerfilListTile(),
            _postPerfilListTile(),
            _postPerfilListTile(),
          ],
        ),
      ),
    ),
    );
  }

   Widget _infoPerfil() {

    String _nFollowing = "376";
    String _nFollowers = "17k";
    String _nFriends   = "305";

    return Container(
      // color: Colors.red, //TODO QUITAR COLOR AL FINAL
      margin: EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            minRadius: 45,
            backgroundImage: AssetImage("assets/images/modelo_perfil.jpg"),
          ),
          Container(
            padding: EdgeInsets.only( left: 10,top: 15),
            child: Column(
              children: <Widget>[ 
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 11.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 3),
                              child: Text("Following")
                            ),
                            Text("$_nFollowing", style: TextStyle(color: Colors.blue)),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 11.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 3),
                              child: Text("Followers")
                            ),
                            Text("$_nFollowers", style: TextStyle(color: Colors.blue)),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 3),
                              child: Text("Friends")
                            ),
                            Text("$_nFriends", style: TextStyle(color: Colors.blue)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 129,top: 3),
                  child: Row(
                    children: <Widget>[
                      Text("Model", style: TextStyle(color: Colors.grey)),
                      Text(" | ", style: TextStyle(color: Colors.grey, fontSize: 12)),
                      Text("Venezuela", style: TextStyle(color: Colors.grey))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 3),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 19),
                        child: RaisedButton(
                          elevation: 4,
                          color: Colors.blue,
                          child: Text("Follow", style: TextStyle(color: Colors.white,fontSize: _fontSizeButton)),
                          onPressed: (){},
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 25),
                        child: OutlineButton(
                          borderSide: BorderSide(
                            width: 1,
                            color: Colors.grey
                          ),
                          child: Text("Pending", style: TextStyle(color: Colors.grey,fontSize: _fontSizeButton)),
                          onPressed: (){},
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _tabBar(){

    String _nPost   = "52";
    String _nBooks  = "52";
    String _nVideos = "5";
    String _nMusic  = "10";
    String _nWorks  = "10";

    double _separacionLetras = 0.7;

    return Container(
      // color: Colors.red, //TODO Quitar color al final
      margin: EdgeInsets.only(right: 10,left: 10),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.0),
        border: Border(bottom: BorderSide(color: Colors.grey, width: 0.8))
      ),
      child: Container(
        margin: EdgeInsets.only(left: 10),
        child: TabBar(
          indicatorSize: TabBarIndicatorSize.label,
          labelPadding: EdgeInsets.all(0),
          labelColor: Colors.blue,
          unselectedLabelColor: Colors.grey,
          indicatorWeight: 2,
          tabs: <Widget>[
            Tab(
              child: Text(
                "Post\n\n"
                "$_nPost",
                textAlign: TextAlign.center,
                style: TextStyle(
                  height: _separacionLetras
                ),
              ),
            ),
            Tab(
              child: Text(
                "Books\n\n"
                "$_nBooks",
                textAlign: TextAlign.center,
                style: TextStyle(
                  height: _separacionLetras
                ),
              ),
            ),
            Tab(
              child: Text(
                "Videos\n\n"
                "$_nVideos",
                textAlign: TextAlign.center,
                style: TextStyle(
                  height: _separacionLetras
                ),
              ),
            ),
            Tab(
              child: Text(
                "Music\n\n"
                "$_nMusic",
                textAlign: TextAlign.center,
                style: TextStyle(
                  height: _separacionLetras
                ),
              ),
            ),
            Tab(
              child: Text(
                "Works\n\n"
                "$_nWorks",
                textAlign: TextAlign.center,
                style: TextStyle(
                  height: _separacionLetras
                ),
              ),
            ),
            Tab(
              icon: Icon(
                Icons.info_outline,
                color: Colors.blue
              )
            )
          ],
        ),
      ),
    );
  }

  Widget _postPerfilListTile() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/images/modelo_perfil.jpg"),
              radius: 30,
            ),
            title: Row(
              children: <Widget>[
                Text("$_nombrePerfil", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),),
                Icon(Icons.check_circle,color: Colors.blue, size: 14,)
              ],
            ),
            subtitle: Text(
              "Model\n\n"
              "10d ago",
              style: TextStyle(
                color: Colors.grey,
                height: 0.5,
              )
            ),
            trailing: Container(
              margin: EdgeInsets.only(top: 20),
              child: IconButton(
                icon: Icon(Icons.more_vert), //TODO Cambiar a icon button
                tooltip: "Opciones",
                onPressed: (){

                },
              ) 
            ),
          ),
          Image(
            image: AssetImage("assets/images/modelo_publicacion.jpg"),
          ),
          Container(
            margin: EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Icon( //TODO Cambiar a icon button
                    Icons.thumb_up,
                    color: Colors.grey,
                  )
                ),
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Text("3",style: TextStyle(color: Colors.grey))
                ),
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Icon( //TODO Cambiar a icon button
                    Icons.comment,
                    color: Colors.grey,
                  )
                ),
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Text("3",style: TextStyle(color: Colors.grey))
                ),
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Icon( //TODO Cambiar a icon button
                    Icons.redo,
                    color: Colors.grey,
                  )
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            child: RichText(
              text: TextSpan(
                style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                text: "$_nombrePerfil ",
                children: <TextSpan>[
                  TextSpan(
                    style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                    text: "$_commentPost"
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10,top: 7),
            child: Text(
              "View al $_nComment comments",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 13,
              ),
            ),
          )
        ],
      ),
    );
  }


}